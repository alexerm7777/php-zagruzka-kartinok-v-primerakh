<?php

error_reporting(E_ALL); ini_set('display_errors', 1);
//определяем положение скрипта на сервере, относительно доменного имени
$web_folder = explode('doimg.php',$_SERVER['PHP_SELF']);
$web_folder_script = $web_folder[0];
$web_folder_upload = $web_folder[0].'uploads/';
//создаем папку "uploads" в папке, где лежит скрипт обработчик, сюда будем закачивать файлы
$server_upload_folder = $_SERVER['DOCUMENT_ROOT'].$web_folder[0].'uploads/';
@mkdir($server_upload_folder, 0777);

//определяем метод загрузки картинок
switch ($_REQUEST['action']){
    case 'onefile':
        onefile();
        break;
    case 'multiple':
        multiple();
        break;
    case 'link':
        weblink();
        break;
    case 'base64':
        base64();
        break;
    case 'base64json':
        base64json();
        break;
}

function draw_img($filename){
    global $web_folder_script;
    echo "<img src='".$web_folder_script."preview.php?src=uploads/".$filename."'><br>";
    echo "<a href='".$web_folder_script."uploads/".$filename."' target='_blank'>открыть оригинал</a><hr>";
}

function onefile(){
    global $server_upload_folder;
    $new_file_name = strtotime("now")."_". basename($_FILES['userfile']['name']);
    $uploadfile = $server_upload_folder . $new_file_name;

    if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
        draw_img($new_file_name);
    } else {
        echo "Возможная атака с помощью файловой загрузки!\n";
    }
}

function multiple(){
    global $server_upload_folder;
    $total = count($_FILES['userfile']['name']);

// Loop through each file
    for($i=0; $i<$total; $i++) {

        //Get the temp file path
        $tmpFilePath = $_FILES['userfile']['tmp_name'][$i];

        //Make sure we have a filepath
        if ($tmpFilePath != ""){
            //Setup our new file path
            $newfilename = strtotime("now") .'_'. basename($_FILES['userfile']['name'][$i]);
            $newFilePath = $server_upload_folder . $newfilename;

            //Upload the file into the temp dir
            if(move_uploaded_file($tmpFilePath, $newFilePath)) {
                draw_img($newfilename);
            }
            else{
                echo "Возможная атака с помощью файловой загрузки!\n";
            }
        }else{
            echo "По каким то причинам файл не был загружен";
        }
    }
}

function weblink(){
    global $server_upload_folder;
    $piclink = $_REQUEST['link'];

//    вытаскиваем имя файла из ссылки и на его основе создаем новое имя
    $picname = explode('/',$piclink);
    $x = count ($picname);
    $newfilename = strtotime("now").'_'.$picname[$x-1];

    $path = $server_upload_folder.$newfilename;
    file_put_contents($path, file_get_contents($piclink));
    draw_img($newfilename);
}

function base64(){
    global $server_upload_folder;

    $base64_string = $_REQUEST['base64'];
    $filename = strtotime("now");
    $output_file = base64_to_img($base64_string, $server_upload_folder, $filename );
    draw_img($output_file['new_name']);
}

function base64json(){
    global $server_upload_folder;
    $json = $_REQUEST['base64json'];
    $jsondecode = json_decode($json, true);

    foreach( $jsondecode as $key => $val ) {
        $base64_string = $val;
        $filename = $key;

        $output_file = base64_to_img($base64_string, $server_upload_folder, $filename );
        draw_img($output_file['new_name']);
    }
}

function base64_to_img($base64_string, $server_upload_folder, $filename) {

    // split the string on commas
    // $data[ 0 ] == "data:image/png;base64"
    // $data[ 1 ] == <actual base64 string>
    $data = explode( ',', $base64_string );

    // вытаскиваем тип обрабатываемого файла
    $filetype = explode('/',$data[0]);
    $filetype = explode(';',$filetype[1]);
    $output_file['filetype'] = $filetype[0];

    $output_file['new_name'] = strtotime("now").'_'.$filename.'.'.$output_file['filetype'];
    $output_file['server_link'] = $server_upload_folder.$output_file['new_name'];

    // open the output file for writing
    $ifp = fopen( $output_file['server_link'], 'wb' );
    // we could add validation here with ensuring count( $data ) > 1
    fwrite( $ifp, base64_decode( $data[ 1 ] ) );
    // clean up the file resource
    fclose( $ifp );

    return $output_file;
}